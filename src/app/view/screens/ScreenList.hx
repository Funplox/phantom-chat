package app.view.screens;
import app.view.screens.mainChat.MainChat;
import app.view.screens.settings.SettingsScreen;
import com.creativemage.screenManager.AScreenList;

/**
 * ...
 * @author Creative Magic
 */
class ScreenList extends AScreenList
{
	private static var MAIN_CHAT:String = "mainChat";
	private static var SETTINGS_SCREEN:String = "settings screen"

	public function new() 
	{
		super();
		
		push( MAIN_CHAT, MainChat);
		push( SETTINGS_SCREEN, SettingsScreen);
	}
	
}