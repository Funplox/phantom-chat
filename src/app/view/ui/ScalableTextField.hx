package app.view.ui;
import openfl.display.Sprite;
import openfl.text.TextField;

/**
 * ...
 * @author Creative Magic
 */
class ScalableTextField extends Sprite
{
	@:value public var wordsPerParahraph(get, null):Int = 5;
	public var text(get, set):String;
	
	private var textField:TextField;
	
	
	public function new() 
	{
		super();
		
		setupTextField();
	}
	
	// PUBLIC METHOD
	
	public function setWordNumPerParagraph(wordsInParagraph:Int):Void
	{
		wordsPerParahraph = wordsInParagraph;
	}
	
	public function update():Void
	{
		
	}
	
	// PRIVATE METHODS
	
	function setupTextField() 
	{
		
	}
	
	
	
	// GETTER AND SETTERS
	
	function get_text():String 
	{
		return text;
	}
	
	function set_text(value:String):String 
	{
		return text = value;
	}
	
	function get_wordsPerParahraph():Int 
	{
		return wordsPerParahraph;
	}
	
}