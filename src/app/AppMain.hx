package app;
import app.view.screens.ScreenList;
import com.creativemage.screenManager.ScreenManager;
import openfl.Lib;

/**
 * ...
 * @author Creative Magic
 */
class AppMain
{
	private var screenManager:ScreenManager;

	public function new() 
	{
		createScreenManager();
	}
	
	function createScreenManager() 
	{
		var w:Int = Lib.current.stage.stageWidth;
		var h:Int = Lib.current.stage.stageHeight;
		
		screenManager = new ScreenManager( Lib.current.stage, w, h, new ScreenList());
	}
	
	public function init() 
	{
		screenManager.init();
	}
	
}