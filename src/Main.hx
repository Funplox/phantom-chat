package;

import app.AppMain;
import lime.app.Application;
import openfl.display.Sprite;
import openfl.Lib;
import openfl.display.StageAlign;
import openfl.display.StageScaleMode;

/**
 * ...
 * @author Creative Magic
 */

class Main extends Sprite
{
	
	public function new()
	{
		super();

		setupStage();
		startupApplication();
	}
	
	// PRIVATE METHODS

	function setupStage():Void
	{
		Lib.current.stage.align = StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = StageScaleMode.NO_SCALE;
	}
	
	function startupApplication() 
	{
		var appInstance = new AppMain();
		appInstance.init();
	}

}

