package ; #if !flash


import openfl._internal.formats.swf.SWFLite;
import openfl.display.MovieClip;
import openfl.Assets;


class SettingsLabelMC extends MovieClip {
	
	
	
	
	public function new () {
		
		super ();
		
		/*
		if (!SWFLite.instances.exists ("GyOWLW2gJiVNR0M1Xvlw")) {
			
			SWFLite.instances.set ("GyOWLW2gJiVNR0M1Xvlw", SWFLite.unserialize (Assets.getText ("GyOWLW2gJiVNR0M1Xvlw")));
			
		}
		*/
		
		var swfLite = SWFLite.instances.get ("GyOWLW2gJiVNR0M1Xvlw");
		var symbol = swfLite.symbols.get (14);
		
		__fromSymbol (swfLite, cast symbol);
		
	}
	
	
}


#else
@:bind @:native("SettingsLabelMC") class SettingsLabelMC extends flash.display.MovieClip {
	
	
	
	
	public function new () {
		
		super ();
		
	}
	
	
}
#end