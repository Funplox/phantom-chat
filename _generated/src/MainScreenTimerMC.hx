package ; #if !flash


import openfl._internal.formats.swf.SWFLite;
import openfl.display.MovieClip;
import openfl.Assets;


class MainScreenTimerMC extends MovieClip {
	
	
	@:keep public var timerTextField (default, null):openfl.text.TextField;
	
	
	public function new () {
		
		super ();
		
		/*
		if (!SWFLite.instances.exists ("GyOWLW2gJiVNR0M1Xvlw")) {
			
			SWFLite.instances.set ("GyOWLW2gJiVNR0M1Xvlw", SWFLite.unserialize (Assets.getText ("GyOWLW2gJiVNR0M1Xvlw")));
			
		}
		*/
		
		var swfLite = SWFLite.instances.get ("GyOWLW2gJiVNR0M1Xvlw");
		var symbol = swfLite.symbols.get (8);
		
		__fromSymbol (swfLite, cast symbol);
		
	}
	
	
}


#else
@:bind @:native("MainScreenTimerMC") class MainScreenTimerMC extends flash.display.MovieClip {
	
	
	@:keep public var timerTextField (default, null):openfl.text.TextField;
	
	
	public function new () {
		
		super ();
		
	}
	
	
}
#end