package ; #if !flash


import openfl._internal.formats.swf.SWFLite;
import openfl.display.MovieClip;
import openfl.Assets;


class ClearAllButtonMC extends MovieClip {
	
	
	
	
	public function new () {
		
		super ();
		
		/*
		if (!SWFLite.instances.exists ("GyOWLW2gJiVNR0M1Xvlw")) {
			
			SWFLite.instances.set ("GyOWLW2gJiVNR0M1Xvlw", SWFLite.unserialize (Assets.getText ("GyOWLW2gJiVNR0M1Xvlw")));
			
		}
		*/
		
		var swfLite = SWFLite.instances.get ("GyOWLW2gJiVNR0M1Xvlw");
		var symbol = swfLite.symbols.get (12);
		
		__fromSymbol (swfLite, cast symbol);
		
	}
	
	
}


#else
@:bind @:native("ClearAllButtonMC") class ClearAllButtonMC extends flash.display.MovieClip {
	
	
	
	
	public function new () {
		
		super ();
		
	}
	
	
}
#end